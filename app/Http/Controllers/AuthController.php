<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function formhtml(){
        return view('formhtml');
    }

    public function datang(Request $request){
        return view('datang');
    }
}
