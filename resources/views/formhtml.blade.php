<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <form action="/datang" method="POST">
    @csrf
        <h3>Sign Up Form</h3>
        <label for="namadepan">First name:</label><br> <br>
        <input type="text" name="fname" id="namadepan"> <br> <br>
        <label for="namabelakang">Last name:</label><br> <br>
        <input type="text" name="lname" id="namabelakang"> <br> <br>

        <label for="">Gender:</label><br> <br>
        <input type="radio" name="gender" id="male" value="male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" id="female" value="female">
        <label for="female">Female</label><br>
        <input type="radio" name="gender" id="other" value="other">
        <label for="other">other</label> <br> <br>

        <label for="">Nationality</label> <br> <br>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="England">England</option>
            <option value="USA">USA</option>
        </select> <br> <br>

         <label for="">Language Spoken</label><br> <br> 
         <input type="checkbox" name="ls" id="ind"> <label for="ind">Bahasa Indonesia</label><br>
         <input type="checkbox" name="ls" id="en"> <label for="en">English</label><br>
         <input type="checkbox" name="ls" id="otr"> <label for="otr">Other</label><br> <br>
         
        <label for="">Bio:</label><br> <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
        <br> <br>
        <input type="submit" value="Submit">
    </form>
    
</body>
</html>