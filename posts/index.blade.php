@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Cast Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                  <a class="btn btn-primary mb-2" href="/posts/cast">Create New Cast</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 20px">No</th>
                      <th>Nama</th>
                      <th>Bio</th>
                      <th>Alamat</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                         @forelse($casts as $key => $cast)
                            <tr>
                                <td>  {{$key + 1}}  </td>
                                <td>  {{$cast->nama}} </td>
                                <td>  {{$cast->bio}} </td>
                                <td>  {{$cast->alamat}} </td>
                                <td style="display: flex"> 
                                    <a href="/post/{{$cast->id}}" class="btn btn-info btn-sm">Show</a>
                                    <a href="/post/{{$cast->id}}/edit" class="btn btn-default btn-sm ml-2">Edit</a>
                                    <form action="/post/{{$cast->id}}" method="get">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="delete" class="btn btn-danger btn-sm ml-2">
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" align="center"> No Cast</td>
                            </tr>
                        @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
    </div>
    </div>
@endsection