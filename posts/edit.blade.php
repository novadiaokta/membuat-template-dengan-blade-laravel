@extends('adminlte.master')

@section('content')

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edit Post {{$cast->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/posts/{{$cast->id}}" method="POST"> 
        @csrf
        @method('PUT')
    <div class="card-body">
        <div class="form-group">
            <label for="nama">Nama Cast</label>
                <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $cast->nama)}}" placeholder="Enter Nama">
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
                <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', $cast->bio)}}" placeholder="Enter Bio">
        @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div>
        <div class="form-group">
            <label for="alamat">Alamat</label>
                <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat', $cast->alamat)}}" placeholder="Enter Alamat">
        @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div>
    </div>  
    <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>

@endsection