<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    return view('test');
});

// Route::get('/form', 'RegisterController@form');

// Route::get('/sapa', 'RegisterController@sapa');

// Route::post('/sapa', 'RegisterController@sapa_post');

Route::get('/home', 'HomeController@home');
Route::get('/formhtml', 'AuthController@formhtml');
Route::get('/datang', 'AuthController@datang');

Route::post('/datang', 'AuthController@datang');

Route::get('/master', function () {
    return view('adminlte.master');
});

Route::get('/items', function () {
    return view('items.index');
});

Route::get('/table', function () {
    return view('adminlte.table');
});

Route::get('/data-table', function () {
    return view('adminlte.data-table');
});

Route::get('/posts/cast', 'PostController@create');
Route::post('/cast', 'PostController@store');
Route::get('/cast', 'PostController@index');
Route::get('/cast/{id}', 'PostController@show');
Route::get('/cast/{id}/edit', 'PostController@edit');
Route::put('/cast/{id}', 'PostController@update');
Route::delete('/cast/{id}', 'PostController@destroy');
